import java.io.*;
import java.util.Stack;

public class CalculatorTest
{
    public static void main(String args[])
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (true)
        {
            try {
                String input = br.readLine();
                if (input.compareTo("q") == 0)
                    break;
                command(input);
            } catch (Exception e) {
                System.out.println("ERROR");
                // e.printStackTrace(); // for debug
            }
        }
    }

    private static void command(String input)
    {
        try {
            InfixExpression infix = new InfixExpression(input);
            String pfInput = infix.convertPostfix();
            PostfixExpression postfix = new PostfixExpression(pfInput);
            long result = postfix.calculate();
            System.out.println(pfInput);
            System.out.println(result);
        } catch (Exception e) {
            System.out.println("ERROR");
            //e.printStackTrace(); // for debug
        }
    }
}

class Expression {
    private static final int PRIORITY_DEFAULT = 0;
    private static final int PRIORITY_PLUS = 1;
    private static final int PRIORITY_MINUS = 1;
    private static final int PRIORITY_MULTIPLY = 2;
    private static final int PRIORITY_DIVIDE = 2;
    private static final int PRIORITY_MOD = 2;
    private static final int PRIORITY_NEGATIVE = 3;
    private static final int PRIORITY_EXPONENT = 4;
    private static final int PRIORITY_PARENTHESIS = 0;
    private static int priority(String op) {
        switch(op) {
        case "+":
            return PRIORITY_PLUS;
        case "-":
            return PRIORITY_MINUS;
        case "*":
            return PRIORITY_MULTIPLY;
        case "/":
            return PRIORITY_DIVIDE;
        case "%":
            return PRIORITY_MOD;
        case "~":
            return PRIORITY_NEGATIVE;
        case "^":
            return PRIORITY_EXPONENT;
        case "(":
            return PRIORITY_PARENTHESIS;
        case ")":
            return PRIORITY_PARENTHESIS;
        default:
            return PRIORITY_DEFAULT;
        }
    }
    public static int compare(String op1, String op2) {
        return priority(op1)-priority(op2);
    }
    public static final String OPERATOR_REGEX = "[\\+\\-\\*\\/\\%\\~\\^\\(\\)]";
    public static final String NUMBER_REGEX = "\\d+";
    protected Stack<String> tokens;
    public Expression(String expr) throws Exception {
        // remark: parse & push input in reverse order to pop in correct order
        tokens = new Stack<String>();
        for(int i=expr.length(); i>0; --i) {
            String token = expr.substring(i-1,i);
            if(token.matches(OPERATOR_REGEX)) {
                tokens.push(token);
            } else if(token.matches(NUMBER_REGEX)) {
                int current = i;
                while(i>=2 && expr.substring(i-2,current).matches(NUMBER_REGEX)) {
                    --i;
                }
                token = expr.substring(i-1,current);
                tokens.push(token);
            } else if(token.matches("\\s")) {
                continue;
            } else {
                throw new IllegalArgumentException(expr);
            }
        }
    }
}

class InfixExpression extends Expression {
    public InfixExpression(String expr) throws Exception {
        super(expr);
    }
    public String convertPostfix() throws Exception {
        String output = "";
        Stack<String> operators = new Stack<String>();
        Stack<String> elements = tokens;
        boolean isPrevNum = false;
        while (!elements.empty()) {
            String token = elements.pop();
            if(token.matches(NUMBER_REGEX)) {
                if(isPrevNum) {
                    throw new IllegalArgumentException();
                }
                output = output.concat(" ").concat(token);
                isPrevNum = true;
            } else if(token.matches(OPERATOR_REGEX)) {
                if(token.equals("-") && !isPrevNum) {
                    token = "~";
                    operators.push(token);
                    isPrevNum = false;
                } else if(token.equals("(")) {
                    operators.push(token);
                } else if(token.equals(")")) {
                    String popped = operators.pop();
                    while(!popped.equals("(")) {
                        output = output.concat(" ").concat(popped);
                        popped = operators.pop();
                    }
                } else {
                    if(token.equals("^")) {
                        while(!operators.empty() && compare(operators.peek(), token) > 0) {
                            output = output.concat(" ").concat(operators.pop());
                        }
                    }
                    else {
                        while(!operators.empty() && compare(operators.peek(), token) >= 0) {
                            output = output.concat(" ").concat(operators.pop());
                        }
                    }
                    operators.push(token);
                    isPrevNum = false;
                }
            }
        }
        while(!operators.empty()) {
            output = output.concat(" ").concat(operators.pop());
        }
        return output.substring(1);
    }
}

class PostfixExpression extends Expression {
    public PostfixExpression(String expr) throws Exception {
        super(expr);
    }
    public long calculate() throws Exception {
        Stack<Long> numbers = new Stack<Long>();
        Stack<String> elements = tokens;
        long a,b,n;
        while(!elements.empty()) {
            String input = elements.pop();
            if(input.matches(OPERATOR_REGEX)) {
                b = numbers.pop();
                if(input.compareTo("~")!=0) {
                    a = numbers.pop();
                    switch(input) {
                    case "+":
                        n = a + b;
                        break;
                    case "-":
                        n = a - b;
                        break;
                    case "*":
                        n = a * b;
                        break;
                    case "/":
                        n = a / b;
                        break;
                    case "%":
                        n = a % b;
                        break;
                    case "^":
                        if(a==0 && b<0) {
                            throw new ArithmeticException("no negative power");
                        }
                        n = (long)Math.pow(a,b);
                        break;
                    default:
                        throw new UnsupportedOperationException(input);
                    }
                } else {
                    n = -b;
                }
                numbers.push(n);
            } else {
                n = Long.parseLong(input);
                numbers.push(n);
            }
        }
        n = numbers.pop();
        if(numbers.empty()) {
            return n;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
