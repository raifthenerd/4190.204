import java.util.ArrayList;
import java.util.Iterator;
import java.io.*;

public class Matching
{
    public static void main(String args[])
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (true)
        {
            try
            {
                String input = br.readLine();
                if (input.compareTo("QUIT") == 0)
                    break;

                command(input);
            }
            catch (IOException e)
            {
                System.out.println("입력이 잘못되었습니다. 오류 : " + e.toString());
            }
        }
    }
    private static Patterns patterns = new Patterns(6);
    private static void command(String input) throws IOException
    {
        // TODO : 아래 문장을 삭제하고 구현해라.
        // System.out.println("<< command 함수에서 " + input + " 명령을 처리할 예정입니다 >>");
        char operator = input.charAt(0);
        String operand = input.substring(2);
        switch(operator) {
        case '<':
            patterns.addFromFile(operand);
            break;
        case '@':
            System.out.println(patterns.searchHash(Integer.parseInt(operand.trim())));
            break;
        case '?':
            System.out.println(patterns.searchPattern(operand));
            break;
        }
    }
}

class MyLinkedList<T> implements Iterable<T> {
    class Node {
        final T item;
        Node next;
        public Node(T item) {
            this.item = item;
            this.next = null;
        }
    }
    class MyLinkedListIterator implements Iterator<T> {
        private MyLinkedList<T> list;
        private int index;
        public MyLinkedListIterator(MyLinkedList<T> list) {
            this.list = list;
            this.index = 0;
        }
        public boolean hasNext() {
            return index < list.size();
        }
        public T next() {
            return list.get(index++);
        }
        public void remove() {
            list.remove(--index);
        }
    }
    private Node head = null;
    private Node tail = null;
    private int size = 0;
    public Iterator<T> iterator() {
        return new MyLinkedListIterator(this);
    }
    public void add(int index, T obj) {
        throw new UnsupportedOperationException();
    }
    public void add(T obj) {
        Node addee = new Node(obj);
        if(tail==null) {
            head = addee;
            tail = addee;
        } else {
            tail.next = addee;
            tail = addee;
        }
        size++;
    }
    public void remove(int index) {
        Node curr = head;
        Node prev = null;
        for(int i=0; i<index; i++) {
            prev = curr;
            curr = curr.next;
        }
        if(curr!=null) {
            if(prev!=null) {
                prev.next = curr.next;
            } else {
                head = curr.next;
            }
            if(tail==curr) {
                tail = prev;
            }
        }
        size--;
    }
    public void removeAll() {
        throw new UnsupportedOperationException();
    }
    public T get(int index) {
        Node curr = head;
        for(int i=0; i<index; i++) {
            curr = curr.next;
        }
        if(curr!=null) return curr.item;
        else throw new IndexOutOfBoundsException();
    }
    public boolean contains(T obj) {
        Node curr = head;
        while(curr!=null) {
            if(curr.item.equals(obj)) {
                return true;
            }
            curr = curr.next;
        }
        return false;
    }
    public boolean isEmpty() {
        return size==0;
    }
    public int size() {
        return size;
    }
    public String toString() {
        Node curr = head;
        if(curr==null) return null;
        StringBuilder sb = new StringBuilder();
        while(true) {
            sb.append(curr.item.toString());
            if(curr.next==null) return sb.toString();
            else curr = curr.next;
            sb.append(' ');
        }
    }
}
class MyAVLTree<K extends Comparable<K>,V> {
    class Node {
        final K key;
        V value;
        Node left, right, parent;
        int height;
        public Node(K key,V value) {
            this.key = key;
            this.value = value;
            this.left = null;
            this.right = null;
            this.parent = null;
            this.height = 1;
        }
    }
    private Node root = null;
    public V get(K key) {
        Node searched = get(key,root);
        if(searched==null) return null;
        else return searched.value;
    }
    private Node get(K key, Node curr) {
        int compare;
        if(curr==null || (compare=curr.key.compareTo(key))==0) return curr;
        else if (compare > 0) return get(key,curr.left);
        else return get(key,curr.right);
    }
    public void put(K key, V value) {
        Node putee = new Node(key,value);
        Node curr = root;
        Node prev = null;
        while(curr!=null) {
            prev = curr;
            if(curr.key.compareTo(key)>0) curr = curr.left;
            else if (curr.key.compareTo(key)<0) curr = curr.right;
            else return;
        }
        putee.parent = prev;
        if(prev==null) root = putee;
        else if (prev.key.compareTo(key)>0) prev.left = putee;
        else prev.right = putee;
        curr = putee;
        while(curr!=null) {
            balance(curr);
            fixHeight(curr);
            curr = curr.parent;
        }
    }
    public void remove(K key) {
        throw new UnsupportedOperationException();
    }
    private int balanceFactor(Node curr) {
        int left = curr.left==null ? 0 : curr.left.height;
        int right = curr.right==null ? 0 : curr.right.height;
        return left-right;
    }
    private void balance(Node curr) {
        if(balanceFactor(curr)==2) {
            if(balanceFactor(curr.left) == -1) {
                leftRotate(curr.left);
            }
            rightRotate(curr);
        } else if (balanceFactor(curr)==-2) {
            if(balanceFactor(curr.right) == 1) {
                rightRotate(curr.right);
            }
            leftRotate(curr);
        }
    }
    private void fixHeight(Node curr) {
        if(curr.left!=null && curr.right!=null) {
            curr.height = 1+Math.max(curr.left.height,curr.right.height);
        } else if (curr.left!=null && curr.right==null) {
            curr.height = 1+curr.left.height;
        } else if (curr.left==null && curr.right!=null) {
            curr.height = 1+curr.right.height;
        } else {
            curr.height = 1;
        }
    }
    private void leftRotate(Node curr) {
        Node child = curr.right;
        curr.right = child.left;
        if(child.left!=null) {
            child.left.parent = curr;
        }
        child.left = curr;
        child.parent = curr.parent;
        if(curr.parent!=null) {
            if(curr.parent.left == curr) {
                curr.parent.left = child;
            } else {
                curr.parent.right = child;
            }
        } else {
            root = child;
        }
        curr.parent = child;
        fixHeight(curr);
        fixHeight(child);
    }
    private void rightRotate(Node curr) {
        Node child = curr.left;
        curr.left = child.right;
        if(child.right!=null) {
            child.right.parent = curr;
        }
        child.right = curr;
        child.parent = curr.parent;
        if(curr.parent!=null) {
            if(curr.parent.left == curr) {
                curr.parent.left = child;
            } else {
                curr.parent.right = child;
            }
        } else {
            root = child;
        }
        curr.parent = child;
        fixHeight(curr);
        fixHeight(child);
    }
    public String toString() {
        StringBuilder sb = toStringBuilder(root);
        if(sb==null) return null;
        else return sb.toString();
    }
    private StringBuilder toStringBuilder(Node node) {
        if(node==null) return null;
        StringBuilder sb = new StringBuilder(node.key.toString());
        StringBuilder tmp;
        if((tmp=toStringBuilder(node.left))!=null)
            sb.append(' ').append(tmp);
        if((tmp=toStringBuilder(node.right))!=null)
            sb.append(' ').append(tmp);
        return sb;
    }
}
class MyHashTable<K extends Comparable<K>,V> {
    final int capacity;
    private ArrayList<MyAVLTree<K,V>> table;
    public MyHashTable(int capacity) {
        this.capacity = capacity;
        this.table = new ArrayList<MyAVLTree<K,V>>(capacity);
        clear();
    }
    public void remove(K key) {
        throw new UnsupportedOperationException();
    }
    public boolean isEmpty() {
        throw new UnsupportedOperationException();
    }
    public int size() {
        throw new UnsupportedOperationException();
    }
    public void clear() {
        table.clear();
        for(int i=0;i<capacity;i++) table.add(i,new MyAVLTree<K,V>());
    }
    public V get(K key) {
        return table.get(key.hashCode()).get(key);
    }
    public void put(K key, V value) {
        table.get(key.hashCode()).put(key,value);
    }
    public String keysAt(int index) {
        return table.get(index).toString();
    }
}

class StringKey implements Comparable<StringKey> {
    final String key;
    public StringKey(String key) {
        this.key = key;
    }
    public int hashCode() {
        char[] arr = key.toCharArray();
        int i,res = 0;
        for(i=0;i<arr.length;i++)
            res = (res+arr[i])%100;
        return res;
    }
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StringKey other = (StringKey) obj;
        if (key == null) {
            if (other.key != null)
                return false;
        } else if (!key.equals(other.key))
            return false;
        return true;
    }
    public int compareTo(StringKey o) {
        return key.compareTo(o.key);
    }
    public String toString() {
        return key;
    }
}
class Tuple {
    final int x,y;
    public Tuple(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Tuple other = (Tuple) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        return true;
    }
    public Tuple add(Tuple o) {
        return new Tuple(x+o.x,y+o.y);
    }
    public String toString() {
        return "("+Integer.toString(x)+", "+Integer.toString(y)+")";
    }
}
class Patterns {
    final int k;
    private MyHashTable<StringKey,MyLinkedList<Tuple>> table;
    public Patterns(int k) {
        this.k = k;
        this.table = new MyHashTable<StringKey,MyLinkedList<Tuple>>(100);
    }
    public void addFromFile(String filename) throws IOException {
        table.clear();
        BufferedReader readtxt = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
        int range, counter = 0;
        String line, subline;
        while((line = readtxt.readLine())!=null) {
            counter++;
            range = line.length()-k+1;
            for(int i=0; i < range; i++) {
                subline = line.substring(i,i+k);
                StringKey key = new StringKey(subline);
                MyLinkedList<Tuple> list = table.get(key);
                if(list!=null) {
                    list.add(new Tuple(counter,i+1));
                } else {
                    list = new MyLinkedList<Tuple>();
                    list.add(new Tuple(counter,i+1));
                    table.put(key,list);
                }
            }
        }
        readtxt.close();
    }
    public String searchHash(int hashValue) {
        final String nullReturn = "EMPTY";
        String result = table.keysAt(hashValue);
        if(result==null) return nullReturn;
        else return result;
    }
    public String searchPattern(String pattern) {
        final String nullReturn = "(0, 0)";
        String substring = pattern.substring(0,k);
        MyLinkedList<Tuple> result = new MyLinkedList<Tuple>();
        MyLinkedList<Tuple> firstTry;
        firstTry = table.get(new StringKey(substring));
        if(firstTry==null) return nullReturn;
        for(Tuple i : firstTry) result.add(i);
        int range = (pattern.length()-1)/k;
        int diff = (pattern.length()-1)%k+1;
        for(int i=0; i<range; i++) {
            substring = pattern.substring(diff+i*k,diff+(i+1)*k);
            MyLinkedList<Tuple> temp = table.get(new StringKey(substring));
            if(temp==null) return nullReturn;
            Iterator<Tuple> it = result.iterator();
            while(it.hasNext()) {
                Tuple curr = it.next();
                Tuple search = curr.add(new Tuple(0,diff+i*k));
                if(!temp.contains(search)) {
                    it.remove();
                }
            }
            if(result.isEmpty()) return nullReturn;
        }
        return result.toString();
    }
}
