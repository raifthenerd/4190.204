#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from random import randint, getrandbits, choice, sample
classname = "BigInteger"
def generate_test_data():
    def rand_num(bits=32,sign=True):
        result = str(getrandbits(bits))[:100]
        if(sign):
            result = choice('+- ')+result
        return result.strip()
    def rand_empty(n):
        return "".join([" " for _ in range(n)])
    def rand_eq(op1,op2,empty=True):
        return rand_empty(randint(0,32)*empty)+op1+rand_empty(randint(0,32)*empty)+choice('+-*')+rand_empty(randint(0,32)*empty)+op2+rand_empty(randint(0,32)*empty)
    def rand_eqq(op,sign=True,empty=True):
        if(sign):
            sign1 = choice('+- ').strip()
            sign2 = choice('+- ').strip()
        else:
            sign1 = ''
            sign2 = ''
        return rand_empty(randint(0,32)*empty)+sign1+op+rand_empty(randint(0,32)*empty)+choice('+-*')+rand_empty(randint(0,32)*empty)+sign2+op+rand_empty(randint(0,32)*empty)

    result = []
    sample_set = []
    per_test = 200
    str_quit = "\nQUIT\n"
    for i in range(per_test):
        sample_set.append(rand_eqq(rand_num(sign=False),sign=False,empty=False))
        sample_set.append(rand_eqq(rand_num(sign=False),sign=False))
        sample_set.append(rand_eqq(rand_num(sign=False),empty=False))
        sample_set.append(rand_eqq(rand_num(sign=False)))
        sample_set.append(rand_eq(rand_num(sign=False),rand_num(sign=False),empty=False))
        sample_set.append(rand_eq(rand_num(sign=False),rand_num(sign=False)))
        sample_set.append(rand_eq(rand_num(),rand_num(),empty=False))
        sample_set.append(rand_eq(rand_num(),rand_num()))
        sample_set.append(rand_eqq(rand_num(512,False),sign=False,empty=False))
        sample_set.append(rand_eqq(rand_num(512,False),sign=False))
        sample_set.append(rand_eqq(rand_num(512,False),empty=False))
        sample_set.append(rand_eqq(rand_num(512,False)))
        sample_set.append(rand_eq(rand_num(512,False),rand_num(512,False),empty=False))
        sample_set.append(rand_eq(rand_num(512,False),rand_num(512,False)))
        sample_set.append(rand_eq(rand_num(512),rand_num(512),empty=False))
        sample_set.append(rand_eq(rand_num(512),rand_num(512)))
    for i in range(per_test):
        for j in [1,3,10,50,200]:
            result.append("\n".join(sample(sample_set,j)))
    result.append("\n".join(sample_set))
    return [test+str_quit for test in result]

def my_line_result(l):
    if(l == "QUIT"):
        return ""
    else:
        return str(eval(l))



# DO NOT MODIFY BELOW LINES!!!

from subprocess import Popen, PIPE, TimeoutExpired
encoding = "UTF-8"

def my_results(s):
    return "\n".join([my_line_result(l) for l in s.splitlines()])

def check(test):
    p = Popen(["java", classname], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    try:
        hw_out, hw_err = p.communicate(input=bytes(test,encoding),timeout=5)
        hw_out = hw_out.decode(encoding)
        hw_err = hw_err.decode(encoding)
    except TimeoutExpired:
        p.kill()
        hw_out = ""
        hw_err = "TIMEOUT!"
    valid_out = my_results(test)
    is_ok = hw_out == valid_out
    if(not is_ok):
        print("SOMETHING WRONG HERE!!")
        print("== Given Input")
        print(test)
        print("== Correct Answer")
        print(valid_out)
        print("== My Answer")
        print(hw_out)
        if(hw_err):
            print(hw_err)
    return is_ok

print("Start test...")
result = [check(d) for d in generate_test_data()]
print("Result: "+str(sum(result))+"/"+str(len(result)))
