import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BigInteger
{
    public static final String QUIT_COMMAND = "quit";
    public static final String MSG_INVALID_INPUT = "입력이 잘못되었습니다.";

    // implement this
    public static final Pattern EXPRESSION_PATTERN = Pattern.compile("\\s*(?<sign1>[+-]?)\\s*(?<operand1>\\d+)\\s*(?<operator>[*+-])\\s*(?<sign2>[+-]?)\\s*(?<operand2>\\d+)\\s*");
    private byte[] digits;
    private boolean sign;
    public byte[] getDigits() { return this.digits; }
    public boolean getSign() { return this.sign; }

    public BigInteger(byte[] digits, boolean sign) {
        int length = digits.length;
        this.digits = new byte[length];
        for(int i=0;i<length;i++) {
            this.digits[i] = digits[i];
        }
        this.sign = sign;
    }
    public BigInteger(String digits, boolean sign)
    {
        int length = digits.length();
        this.digits = new byte[length];
        for(int i=0;i<length;i++) {
            this.digits[i] = (byte)Character.getNumericValue(digits.charAt(i));
        }
        this.sign = sign;
    }

    public BigInteger add(BigInteger big)
    {
        byte[] digits1 = this.digits;
        byte[] digits2 = big.getDigits();
        boolean sign1 = this.sign;
        boolean sign2 = big.getSign();

        if(sign1==sign2) {
            return new BigInteger(arrayAdd(digits1,digits2),sign1);
        } else {
            if(compare(digits1,digits2)>=0) {
                return new BigInteger(arraySubtract(digits1,digits2),sign1);
            } else {
                return new BigInteger(arraySubtract(digits2,digits1),sign2);
            }
        }
    }

    public BigInteger subtract(BigInteger big)
    {
        byte[] digits1 = this.digits;
        byte[] digits2 = big.getDigits();
        boolean sign1 = this.sign;
        boolean sign2 = big.getSign();

        if(sign1==sign2) {
            if(compare(digits1,digits2)>=0) {
                return new BigInteger(arraySubtract(digits1,digits2),sign1);
            } else {
                return new BigInteger(arraySubtract(digits2,digits1),!sign2);
            }
        } else {
            return new BigInteger(arrayAdd(digits1,digits2),sign1);
        }
    }

    public BigInteger multiply(BigInteger big)
    {
        byte[] digits1 = this.digits;
        byte[] digits2 = big.getDigits();
        boolean sign1 = this.sign;
        boolean sign2 = big.getSign();

        return new BigInteger(arrayMultiply(digits1,digits2),sign1!=sign2);
    }

    @Override
    public String toString()
    {
        if(isZero(this.digits)) return "0";
        String result = this.sign ? "-" : "";
        for(int i=0;i<this.digits.length;i++) {
            result = result.concat(String.valueOf(digits[i]));
        }
        return result;
    }

    // Notation: nat := {0,1,2,...}
    // Initialize 0 array
    private static byte[] init(int length) {
        byte[] output = new byte[length];
        for(int i = 0; i < length; i++) {
            output[i] = 0;
        }
        return output;
    }
    // Check the nat is zero or not
    private static boolean isZero(byte[] data) {
        for(int i = 0; i < data.length; i++) {
            if(data[i]!=0) {
                return false;
            }
        }
        return true;
    }
    // compare two nats : a > b returns 1, a == b returns 0, a < b returns -1
    private static int compare(byte[] a, byte[] b) {
        if(isZero(a) || isZero(b)) {
            return isZero(a) ? (isZero(b) ? 0 : -1): 1;
        } else{ // both integers are positive
            if(a.length != b.length) {
                return a.length > b.length ? 1 : -1;
            }
            for(int i = 0; i < a.length; i++) {
                if(a[i] != b[i]) {
                    return a[i] > b[i] ? 1 : -1;
                }
            }
            return 0;
        }
    }
    // Remove unnecessary 0 in array
    private static byte[] clean(byte[] data) {
        int num = 0;
        if(isZero(data)) {
            return init(1);
        }
        while(num < data.length) {
            if(data[num]!=0) {
                break;
            }
            num++;
        }
        if(num==0) {
            return data;
        }

        byte[] output = init(data.length-num);
        for(int i = 0; i < output.length; i++) {
            output[i] = data[i+num];
        }
        return output;
    }
    // Reverse nat array
    private static byte[] reverse(byte[] data) {
        int length = data.length;
        byte[] output = init(length);
        for(int i = 0; i < length; i++) {
            output[i] = data[length-i-1];
        }
        return output;
    }

    // nat addition
    private static byte[] arrayAdd(byte[] a, byte[] b) {
        if(compare(a,b)==-1) {
            return arrayAdd(b,a);
        }
        a = reverse(a);
        b = reverse(b);
        byte[] output = init(a.length + 1);
        byte carryFlag = 0;
        byte n,na,nb;
        for(int i = 0; i < a.length || carryFlag > 0; i++) {
            na = i < a.length ? a[i] : 0;
            nb = i < b.length ? b[i] : 0;
            n = (byte)(na + nb + carryFlag);
            output[i] = (byte)(n%10);
            carryFlag = (byte)(n/10);
        }
        return clean(reverse(output));
    }
    // nat subtraction, where a >= b
    private static byte[] arraySubtract(byte[] a, byte[] b) {
        if (compare(a,b)==0) {
            return init(1);
        }
        a = reverse(a);
        b = reverse(b);
        byte[] output = init(a.length);
        byte carryFlag = 0;
        byte n,na,nb;
        for(int i = 0; i < a.length; i++) {
            na = i < a.length ? a[i] : 0;
            nb = i < b.length ? b[i] : 0;
            n = (byte)(na - nb + carryFlag);
            if(n >= 0) {
                output[i] = n;
                carryFlag = 0;
            } else {
                output[i] = (byte)(n+10);
                carryFlag = -1;
            }
        }
        return clean(reverse(output));
    }
    // nat multiplication
    private static byte[] arrayMultiply(byte[] a, byte[] b) {
        if(isZero(a) || isZero(b)) {
            return init(1);
        }
        a = reverse(a);
        b = reverse(b);
        byte[] output = init(a.length + b.length);
        byte carryFlag = 0;
        byte n,na,nb;
        for(int i = 0; i < a.length; i++) {
            byte[] tmp = init(i+1+b.length);
            na = i < a.length ? a[i] : 0;
            for(int j = 0; j < b.length || carryFlag > 0; j++) {
                nb = j < b.length ? b[j] : 0;
                n = (byte)(na * nb + carryFlag);
                tmp[i+j] = (byte)(n%10);
                carryFlag = (byte)(n/10);
            }
            output = arrayAdd(output, reverse(tmp));
        }
        return clean(output);
    }

    static BigInteger evaluate(String input) throws IllegalArgumentException
    {
        Matcher filtered = EXPRESSION_PATTERN.matcher(input);
        if (!filtered.matches()) {
            throw new IllegalArgumentException("Given input is not valid.");
        }

        String operator = filtered.group("operator");
        boolean sign1 = filtered.group("sign1").compareTo("-")==0;
        BigInteger operand1 = new BigInteger(filtered.group("operand1"), sign1);
        boolean sign2 = filtered.group("sign2").compareTo("-")==0;
        BigInteger operand2 = new BigInteger(filtered.group("operand2"), sign2);

        switch(operator) {
        case "+":
            return operand1.add(operand2);
        case "-":
            return operand1.subtract(operand2);
        case "*":
            return operand1.multiply(operand2);
        default:
            throw new IllegalArgumentException(operator);
        }
    }

    public static void main(String[] args) throws Exception
    {
        try (InputStreamReader isr = new InputStreamReader(System.in))
        {
            try (BufferedReader reader = new BufferedReader(isr))
            {
                boolean done = false;
                while (!done)
                {
                    String input = reader.readLine();

                    try
                    {
                        done = processInput(input);
                    }
                    catch (IllegalArgumentException e)
                    {
                        System.err.println(MSG_INVALID_INPUT);
                    }
                }
            }
        }
    }

    static boolean processInput(String input) throws IllegalArgumentException
    {
        boolean quit = isQuitCmd(input);

        if (quit)
        {
            return true;
        }
        else
        {
            BigInteger result = evaluate(input);
            System.out.println(result.toString());

            return false;
        }
    }

    static boolean isQuitCmd(String input)
    {
        return input.equalsIgnoreCase(QUIT_COMMAND);
    }
}
