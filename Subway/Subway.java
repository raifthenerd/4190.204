import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.PriorityQueue;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Subway {
    final static Pattern cmd = Pattern.compile("(\\S+)\\s(\\S+)(\\s!)?");
    public static void main(String[] args) {
        SubwayGraph subway = new SubwayGraph();
        BufferedReader b;
        try {
            if(args.length!=1)
                throw new Exception();
            b = new BufferedReader(new InputStreamReader(new FileInputStream(args[0])));
            String line;
            while((line=b.readLine())!=null) {
                if(line.compareTo("")==0)
                    break;
                String[] info = line.split("\\s");
                if(info.length!=3)
                    throw new Exception();
                subway.addStation(info[0],info[1]);
            }
            while((line=b.readLine())!=null) {
                String[] info = line.split("\\s");
                if(info.length!=3)
                    throw new Exception();
                int time = Integer.parseInt(info[2]);
                if(time<0)
                    throw new Exception();
                subway.addEdge(info[0],info[1],time);
            }
            subway.addTransfer();
            b.close();
        } catch (Exception e) {
            System.out.println(e.toString());
            return;
        }
        b = new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            try {
                String input = b.readLine();
                if(input.compareTo("QUIT")==0)
                    break;
                Matcher isCmd = cmd.matcher(input);
                if(!isCmd.matches())
                    throw new IOException();
                String from = isCmd.group(1);
                String to = isCmd.group(2);
                boolean transfer = isCmd.group(3)!=null;
                System.out.println(subway.shortest(from,to,transfer));
            } catch (IOException e) {
                System.out.println(e.toString());
            }
        }
    }
}

class Station implements Comparable<Station> {
    final String id, name;
    public Station(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public int compareTo(Station other) {
        return id.compareTo(other.id);
    }
    public String toString() {
        return name;
    }
}
class Cost implements Graph.Weight<Cost>, Comparable<Cost> {
    final int time, trans;
    static boolean transPrior = false;
    public Cost(int time, int trans) {
        this.time = time;
        this.trans = trans;
    }
    public Cost add(Cost other) {
        return new Cost(time+other.time, trans+other.trans);
    }
    public int compareTo(Cost other) {
        if(transPrior)
            return trans==other.trans ? time-other.time : trans-other.trans;
        else
            return time-other.time;
    }
    public String toString() {
        return Integer.toString(time);
    }
}
class CostFactory implements Graph.WeightFactory<Cost> {
    public Cost minValue() {
        return new Cost(0,0);
    }
    public Cost maxValue() {
        return new Cost(Integer.MAX_VALUE, Integer.MAX_VALUE);
    }
}
class SubwayGraph {
    final static int TRANS_TIME = 5;
    final Graph<Cost,Station> graph = new Graph<Cost,Station>(new CostFactory());
    final Map<String,List<Station>> nameToIds = new TreeMap<String,List<Station>>();
    public void addStation(String id, String name) {
        Station s = new Station(id,name);
        graph.addVertex(s);
        if(!nameToIds.containsKey(name))
            nameToIds.put(name, new ArrayList<Station>());
        nameToIds.get(name).add(s);
    }
    public void addEdge(String from, String to, int time) {
        Station fromKey = new Station(from,null);
        Station toKey = new Station(to,null);
        graph.addEdge(fromKey,toKey,new Cost(time,0));
    }
    public void addTransfer() {
        for(List<Station> ids : nameToIds.values()) {
            int size = ids.size();
            if(size>1) {
                int i,j;
                for(i=0;i<size-1;i++) {
                    for(j=i+1;j<size;j++) {
                        Station s1 = ids.get(i);
                        Station s2 = ids.get(j);
                        graph.addEdge(s1,s2,new Cost(TRANS_TIME,1));
                        graph.addEdge(s2,s1,new Cost(TRANS_TIME,1));
                    }
                }
            }
        }
    }
    public String shortest(String from, String to, boolean transPrior) {
        Cost.transPrior = transPrior;
        return graph.getShortest(nameToIds.get(from),nameToIds.get(to));
    }
}

class Graph<E extends Graph.Weight<E> & Comparable<E>,
            V extends Comparable<V>> {
    public static interface Weight<T> {
        public T add(T other);
    }
    public static interface WeightFactory<T extends Weight> {
        public T minValue();
        public T maxValue();
    }
    private class Vertex implements Comparable<Vertex> {
        final V info;
        final List<Edge> adjs = new ArrayList<Edge>();
        E minCost = factory.minValue();
        Vertex prev = null;
        public Vertex(V info) {
            this.info = info;
        }
        public int compareTo(Vertex other) {
            return minCost.compareTo(other.minCost);
        }
        public String toString() {
            return info.toString();
        }
    }
    private class Edge {
        final Vertex target;
        final E cost;
        public Edge(Vertex target, E cost) {
            this.target = target;
            this.cost = cost;
        }
    }
    private class Path {
        List<Vertex> stations;
        E total;
        public Path(List<Vertex> stations, E total) {
            this.stations = stations;
            this.total = total;
        }
        public String toString() {
            String result = "\n" + total.toString();
            Vertex v;
            Iterator<Vertex> it = stations.iterator();
            String tmp;
            v = it.next();
            while(it.hasNext()) {
                tmp = v.toString();
                v = it.next();
                if(tmp.compareTo(v.toString())==0) {
                    result = " [" + tmp + "]" + result;
                    v = it.next();
                } else {
                    result = " " + tmp + result;
                }
            }
            return v.toString() + result;
        }
    }
    final Map<V,Vertex> vertices = new TreeMap<V,Vertex>();
    final WeightFactory<E> factory;
    public Graph(WeightFactory<E> factory) {
        this.factory = factory;
    }
    public void addVertex(V info) {
        vertices.put(info,new Vertex(info));
    }
    public void addEdge(V from, V to, E cost) {
        vertices.get(from).adjs.add(new Edge(vertices.get(to),cost));
    }
    // modification from www.algolist.com/code/java/Dijkstra%27s_algorithm
    private Path dijkstra(List<Vertex> source, List<Vertex> target) {
        Path result = null;
        for(Vertex s : source) {
            for(Vertex v : vertices.values()) {
                v.prev = null;
                v.minCost = factory.maxValue();
            }
            s.minCost = factory.minValue();
            PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
            vertexQueue.add(s);
            while(!vertexQueue.isEmpty()) {
                Vertex u = vertexQueue.poll();
                if(target.contains(u)) {
                    if(result==null || result.total.compareTo(u.minCost)>0) {
                        List<Vertex> tmp = new ArrayList<Vertex>();
                        for(Vertex v = u; v != null; v = v.prev) tmp.add(v);
                        result = new Path(tmp,u.minCost);
                    }
                    break;
                }
                for(Edge e : u.adjs) {
                    Vertex v = e.target;
                    E costToU = e.cost.add(u.minCost);
                    if(costToU.compareTo(v.minCost)<0) {
                        vertexQueue.remove(v);
                        v.minCost = costToU;
                        v.prev = u;
                        vertexQueue.add(v);
                    }
                }
            }
        }
        return result;
    }
    public String getShortest(List<V> sources, List<V> targets) {
        List<Vertex> vSources = new ArrayList<Vertex>();
        for(V s : sources) vSources.add(vertices.get(s));
        List<Vertex> vTargets = new ArrayList<Vertex>();
        for(V t : targets) vTargets.add(vertices.get(t));
        return dijkstra(vSources,vTargets).toString();
    }
}
