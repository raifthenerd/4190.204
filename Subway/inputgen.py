from random import choice, sample

files = ['subway','busan','oneway','overlap','huge']
for file in files:
    with open(file+'.txt','r') as f:
        st = set(map(lambda x: x.split()[1],
                     f.read().split('\n\n')[0].split('\n')))
    cmds = '\n'.join(' '.join(sample(st,2))+choice([' !',''])
                     for _ in range(100))
    with open(file+'_stdin.txt','w') as f:
        f.write(cmds+'\nQUIT\n')
