import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyLinkedListIterator<T extends Comparable<T>> implements Iterator<T> {
    @SuppressWarnings("unused")
    private Node<T> current;
    public MyLinkedListIterator(Node<T> position) {
        this.current = new Node<T>(null);
        this.current.next = position;
    }
    @Override
    public boolean hasNext() {
        return current.next != null;
    }
    @Override
    public T next() {
        if(hasNext()) {
            current = current.next;
            return current.item;
        } else {
            throw new NoSuchElementException();
        }
    }
    @Override
    public void remove() {
        // This code does not have to be modified.
        throw new UnsupportedOperationException();
    }
}
