import java.util.Iterator;
import java.util.NoSuchElementException;

class Node<T> {
    final T item;
    Node<T> next;
    public Node(T obj) {
        this.item = obj;
        this.next = null;
    }
}
public class MyLinkedList<T extends Comparable<T>> implements Iterable<T> {
    private Node<T> head = null;
    @Override
    public Iterator<T> iterator() {
        return new MyLinkedListIterator<T>(this.head);
    }
    public T add(T obj) {
        Node<T> curr = head;
        Node<T> prev = null;
        while(curr!=null && obj.compareTo(curr.item)>0) {
            prev = curr;
            curr = curr.next;
        }
        if(curr!=null && obj.compareTo(curr.item)==0) {
            return curr.item;
        }
        Node<T> addee = new Node<T>(obj);
        addee.next = curr;
        if(prev!=null) {
            prev.next = addee;
        } else {
            head = addee;
        }
        return obj;
    }
    public T remove(T obj) {
        Node<T> curr = head;
        Node<T> prev = null;
        while(curr!=null && obj.compareTo(curr.item)!=0) {
            prev = curr;
            curr = curr.next;
        }
        if(curr!=null) {
            if(prev!=null) {
                prev.next = curr.next;
            } else {
                head = curr.next;
            }
            return curr.item;
        }
        return null;
    }
    public int size() {
        Node<T> current = head;
        int size = 0;
        while(current!=null) {
            ++size;
            current = current.next;
        }
        return size;
    }
    public T first() {
        // This is a helper method.
        // You do not necessarily have to implement this but still might be useful to do so.
        if (head != null)
            return head.item;
        else
            throw new NoSuchElementException();
    }
    public T last() {
        // This is a helper method.
        // You do not necessarily have to implement this but still might be useful to do so.
        throw new UnsupportedOperationException();
    }
}
