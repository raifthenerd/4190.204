import java.util.Arrays;

public class PrintCmd extends AbstractCommand {
    @Override
    protected void queryDatabase(MovieDatabase db, String[] arga) throws DatabaseException {
        checkArga(arga);
        MyLinkedList<QueryResult> result = db.search(null);
        boolean empty = true;
        for (QueryResult item: result) {
            empty = false;
            System.out.println(item.toString());
        }
        if(empty) {
            System.out.println("EMPTY");
        }
    }
    private void checkArga(String[] arga) throws DatabaseException {
        if (arga.length != 0)
            throw new CommandParseException("PRINT", Arrays.toString(arga), "unnecessary argument(s)");
    }
}
