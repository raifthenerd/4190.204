import java.util.Iterator;

class Genre implements Comparable<Genre> {
    private final String genre;
    private MyLinkedList<String> titles;
    public Genre(String name) {
        this.genre = name;
        this.titles = new MyLinkedList<String>();
    }
    public String getName() {
        return genre;
    }
    public Iterator<String> iterator() {
        return titles.iterator();
    }
    public String add(String title) {
        return titles.add(title);
    }
    public String remove(String title) {
        return titles.remove(title);
    }
    public int size() {
        return titles.size();
    }
    public void search(String query, MyLinkedList<QueryResult> result) {
        for(String title : titles) {
            if(query == null || title.contains(query)) {
                result.add(new QueryResult(genre, title));
            }
        }
    }
    @Override
    public int compareTo(Genre other) {
        return genre.compareTo(other.getName());
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Genre other = (Genre) obj;
        if (genre == null) {
            if (other.genre != null)
                return false;
        } else if (!genre.equals(other.genre))
            return false;
        return true;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((genre == null) ? 0 : genre.hashCode());
        return result;
    }
    @Override
    public String toString() {
        throw new UnsupportedOperationException();
    }
}
