import java.util.Iterator;

public class MovieDatabase {
    private MyLinkedList<Genre> genres;
    public MovieDatabase() {
        this.genres = new MyLinkedList<Genre>();
    }
    public void insert(String genre, String title) {
        Genre addee = new Genre(genre);
        genres.add(addee).add(title);
    }
    public void delete(String genre, String title) {
        Genre removee = new Genre(genre);
        for(Genre curr: genres) {
            if(curr.compareTo(removee)==0) {
                curr.remove(title);
                if(curr.size()==0) {
                    genres.remove(removee);
                }
                return;
            }
        }
    }
    public MyLinkedList<QueryResult> search(String term) {
        MyLinkedList<QueryResult> results = new MyLinkedList<QueryResult>();
        for(Genre genre: genres) {
            genre.search(term, results);
        }
        return results;
    }
}
