import java.util.Arrays;

public class SearchCmd extends AbstractCommand {
    @Override
    protected void queryDatabase(MovieDatabase db, String[] arga) throws DatabaseException {
        checkArga(arga);
        MyLinkedList<QueryResult> result = db.search(arga[0]);
        boolean empty = true;
        for (QueryResult item: result) {
            empty = false;
            System.out.println(item.toString());
        }
        if(empty) {
            System.out.println("EMPTY");
        }
    }
    private void checkArga(String[] arga) throws DatabaseException {
        if (arga.length != 1)
            throw new CommandParseException("SEARCH", Arrays.toString(arga), "insufficient argument");
    }
}
