#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from faker import Faker
fake = Faker()

from random import randint, getrandbits, choice
classname = "MovieDatabaseConsole"

cmds = ["INSERT"]*9+["DELETE"]*9+["SEARCH"]*1+["PRINT"]*1
genres = [fake.city().upper() for _ in range(20)]
genres.append("   ")
names = [fake.name().upper() for _ in range(500)]
names.append("     ")

def generate_instruction():
    def rand_empty(n):
        return "".join([" " for _ in range(randint(0,n))])
    def construct(*args):
        n = 5
        result = rand_empty(n)
        for s in args:
            result = result + s + rand_empty(n)
        return result
    cmd = choice(cmds)
    if cmd == "INSERT" or cmd=="DELETE":
        genre = "%" + rand_empty(1) + choice(genres) + rand_empty(1) + "%"
        name= "%" + rand_empty(1) + choice(names) + rand_empty(1) + "%"
        return construct(cmd,genre,name)
    elif cmd == "SEARCH":
        return construct(cmd,"%"+fake.lexify(text=choice(["?","??"])).upper()+"%")
    return construct(cmd)

def generate_test_data():
    sample_set = [generate_instruction() for _ in range(10000)]
    result = []
    str_quit = "\nQUIT\n"
    for _ in range(30):
        for i in [1000,2500,5000]:
            result.append("\n".join(choice(sample_set) for _ in range(i)))
    return [test+str_quit for test in result]

import re
def my_line_result(l,db):
    l = l.strip()
    cmd = l[:6].strip()
    arg = l[6:].strip()
    if cmd == "QUIT":
        return ""
    elif cmd == "PRINT":
        db.sort()
        search = ["("+i[0]+", "+i[1]+")" for i in db]
        if (search == []):
            return "EMPTY"
        return "\n".join(search)
    elif cmd == "SEARCH":
        arg = arg.strip("%")
        db.sort()
        search = ["("+i[0]+", "+i[1]+")" for i in db if i[1].find(arg) > -1]
        if (search == []):
            return "EMPTY"
        return "\n".join(search)
    elif cmd == "INSERT":
        arg = re.sub("%\s+%", "%%", arg.strip("%")).split("%%")
        if db.count(arg) is 0:
            db.append(arg)
        return None
    elif cmd == "DELETE":
        arg = re.sub("%\s+%", "%%", arg.strip("%")).split("%%")
        if db.count(arg) is 1:
            db.remove(arg)
        return None


# DO NOT MODIFY BELOW LINES!!!

from difflib import unified_diff
from subprocess import Popen, PIPE, TimeoutExpired
encoding = "UTF-8"

def my_results(s):
    db = []
    return "\n".join(filter(lambda x: x is not None,
                            [my_line_result(l,db) for l in s.splitlines()]))

def check(test):
    p = Popen(["java", classname], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    try:
        hw_out, hw_err = p.communicate(input=bytes(test,encoding),timeout=5)
        hw_out = hw_out.decode(encoding)
        hw_err = hw_err.decode(encoding)
        valid_out = my_results(test)
        is_ok = hw_out == valid_out
        if(not is_ok):
            print("SOMETHING WRONG HERE!!")
            #print("== Given Input")
            #print(test)
            #[print(line) for line in unified_diff(valid_out.splitlines(),hw_out.splitlines(),
            #                                      fromfile="Correct",tofile="My Answer")]
            if(hw_err):
                print(hw_err)
        return is_ok
    except TimeoutExpired:
        p.kill()
        print("TIMEOUT!")
        return False

print("Start test...")
result = [check(d) for d in generate_test_data()]
print("Result: "+str(sum(result))+"/"+str(len(result)))
