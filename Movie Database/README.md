# Movie Database

## 개요

이번 과제에서는 Linked List를 사용해서 영화의 장르와 제목이 저장되는 데이터베이스를 구현합니다. 이 데이터베이스에서는 삽입, 삭제, 검색 연산이 가능해야하며, 각 항목이 영화의 장르와 제목에 따라 정렬된 순서로 저장되어야 합니다. Linked List를 이해하는 것이 과제의 목적입니다.

## [뼈대코드](https://bitbucket.org/raifthenerd/dsproject/src/fab327b/Movie Database/MovieDatabase.java)

주어진 스켈레톤 코드는 코드를 구조화할 수 있는 한 가지 방법을 보여주고 있다.
과제를 완료하기 위해 `FIXME`로 표시된 부분의 코드를 채워 넣는 것으로 동작하는 프로그램을 만들 수 있다.
필요하다면 `FIXME`로 표시되지 않은 부분에 코드를 추가하는 것도 가능하다.
스켈레톤 코드는 과제를 시작하는 시작점을 제공할 뿐이다.

빠른 피드백 루프를 위해 테스트를 단계별로 수행하는 것이 유용하다.
이를 위해 일부 테스트 코드가 `AssignmentGuide.java` 파일에 제공되어 있다.

### FIXMEs & TODOs

- `AbstractCommand.java`
- `Genre.java`
- `MovieDatabase.java`
- `MyLinkedListIterator.java`
- `MyLinkedList.java`
- `QueryResult.java`

### Compilation and Execution

#### Compilation

컴파일은 아래와 같이 할 수 있다.

```
javac *.java
```

#### Execution

과제 채점은 다음과 같이 이루어진다.

```
java MovieDatabaseConsole
```

#### Test

과제 코드를 작성하며 도움이 될만한 테스트 몇 개가 준비되어 있다. 이는 아래와 같이 실행한다.

```
java -ea AssignmentGuide
```

`-ea` 옵션은 `assert` 문을 사용하기 위해 필요하다.

#### Eclipse

이클립스에서 스켈레톤 코드를 임포트하는 방법은 아래와 같다.

- File - New Project
- Java - Java Project - Next
- Project name을 정한다 - 적당한 Location을 정한다 - Project layout의 Use project folder as root for sources and class files을 선택한다 - Next
- Finish
- 프로젝트 이름 (가령 MovieDatabase) 우클릭 - Import
- General - File System
- From directory에서 스켈레톤 코드가 위치한 디렉토리를 선택한다 - `.java` 파일을 모두 선택한다 - Finish

### Contributors

This project is contributed by the following people (in alphabetical order).

- ipkn <ipknhama AT gmail DOT com>
- shurain <shurain AT gmail DOT com>
- stania <stania.pe.kr AT gmail DOT com>
- wookayin <wookayin AT gmail DOT com>

## 데이터베이스 구조

우선 각 장르마다 영화 제목에 따라 정렬된 리스트를 만듭니다.

    (ACTION, BATMAN BEGINS, ONG-BAK, THE MATRIX)
    (DRAMA, MILLION DOLLAR BABY, THE AVIATOR)
    (HORROR, HELLRAISER)

그리고 위의 리스트들을 원소로 가지며, 장르에 따라 정렬된 리스트를 만듭니다.

    ( (ACTION, BATMAN BEGINS, ONG-BAK, THE MATRIX), (DRAMA, MILLION DOLLAR BABY, THE AVIATOR), (HORROR, HELLRAISER) )


## 지원하는 명령어

1. 삽입: INSERT %장르% %제목%

    해당 장르와 제목을 가진 영화를 삽입합니다. 이미 데이터베이스에 있는 영화와 장르, 제목이 모두 같으면 삽입하지 않습니다.

2. 삭제: DELETE %장르% %제목%

    해당 장르와 제목을 가진 영화를 삭제합니다. 각 장르의 마지막 영화가 삭제되면 장르도 삭제됩니다.
    
3. 검색: SEARCH %검색어%

    제목에 검색어가 들어 있는 모든 영화들의 장르와 제목을 정렬된 순서로 출력합니다.
    
4. 출력: PRINT

    데이터베이스의 전체 내용을 정렬된 순서로 출력합니다.
    
5. 종료: QUIT

    프로그램을 종료합니다.


## 입출력 형식

1. 프로그램을 실행하면 한줄로 된 명령어를 입력받습니다.
2. 명령어를 입력받으면 적절한 작업을 수행하고 SEARCH와 PRINT는 그 결과를 출력합니다.
3. 명령어의 앞뒤 및 명령어와 인자들 사이에는 0개 이상의 공백이 들어갈 수 있습니다.
4. 모든 영화들의 장르와 제목에는 '%'와 ',' 문자가 들어가지 않습니다.
5. 영화 목록을 출력할 때에는 한 줄에 (장르, 제목)과 같이 하나의 영화만 출력하고, 결과가 더 있으면 다음 줄에 계속 해서 같은 형식으로 출력합니다. 데이터베이스가 비어있으면 EMPTY로 출력합니다.
6. SEARCH시에 검색어가 들어 있는 영화가 없다면, EMPTY로 출력합니다.
7. 모든 입력은 대문자로 들어온다고 가정하셔도 좋습니다.
8. 영화 목록을 정렬 할 때에는 장르로 먼저 정렬하고, 장르가 같을 때에는 제목으로 정렬합니다. 참고로 데이터베이스를 제대로 구현한다면 목록을 정렬하기 위해서 별도의 작업은 필요치 않습니다.
9. 출력할 때에는 한줄 한줄이 괄호로 묶여있어야 하며 ,(콤마) 뒤의 공백 한칸을 반드시 띄워야 합니다. 아래 예시의 형식과 정확히 일치하게 만드세요.
10. 한 명령어의 실행이 끝나면 QUIT를 입력받을때까지 다시 입력을 받습니다.


        $ java MovieDatabase                        <- 프로그램 실행
        INSERT %ACTION% %BATMAN BEGINS%             <- 이렇게 입력
        INSERT %ACTION% %THE MATRIX%                <- 이렇게 입력
        INSERT %DRAMA% %MILLION DOLLAR BABY%        <- 이렇게 입력
        SEARCH %BA%                                 <- 이렇게 입력하면
        (ACTION, BATMAN BEGINS)                     <- 이렇게 출력
        (DRAMA, MILLION DOLLAR BABY)                <- 이렇게 출력한다.
        DELETE %DRAMA% %MILLION DOLLAR BABY%        <- 이렇게 입력
        PRINT                                       <- 이렇게 입력하면
        (ACTION, BATMAN BEGINS)                     <- 이렇게 출력
        (ACTION, THE MATRIX)                        <- 이렇게 출력한다.
        QUIT                                        <- 이렇게 입력하면
        $                                           <- 종료한다.

## 참고사항

- 장르와 영화 제목의 저장에는 Linked List만 사용하도록 합니다. Linked List는 직접 구현해야 합니다.
- 프롬프트 (>)를 출력할 필요는 없습니다. 입력만 잘 받으면 됩니다. 프롬프트를 출력할 경우 출력결과가 이상해질 수도 있습니다.
- 위의 명령어 형식을 잘 따르는 올바른 입력만 들어온다고 가정하셔도 좋습니다. 참고로 명령어 형식을 따른 중복 삽입이나 데이터베이스에 없는 영화 삭제 및 검사 등은 올바른 입력에 해당합니다. 이러한 경우에는 위에서 설명한대로 적절한 조치를 취하시면 됩니다. (데이터베이스에 없는 영화 삭제나 중복 삽입은 따로 아무 메시지를 출력하지 않고 처리하면 됩니다.)
- 주어진 MovieDatabase 클래스 내에서 구현하여 `$ java MovieDatabase` 같은 식으로 실행할 수 있도록 합니다. 클래스 내에서 함수를 더 추가하거나 private 클래스를 더 만드는건 상관 없습니다.
- **과목 게시판에 과제에 대한 질문 및 주의사항이 공지됩니다. 이를 수시로 확인하시기 바랍니다.**
- 부정행위에 관한 주의사항을 읽기 바랍니다.

## FAQ

- 명령어의 종류

    문제에 제시된 명령어 외의 명령어는 들어오지 않습니다.

- 공백의 처리

    명령어 사이 및 명령어 앞뒤에도 공백이 올 수 있습니다. 제목의 앞뒤에도 공백이 올 수 있는데 이는 제목에 포함된 문자로 처리해 주세요. 즉, `INSERT %   ACTION  % %  THE MATRIX  %`는 '   ACTION  ' 장르의 '  THE MATRIX  ' 영화를 삽입하는 명령입니다. 이는 'ACTION' 장르의 'THE MATRIX' 영화와 다릅니다.
    
- 대소문자 처리
    
    모든 명령어 및 제목은 대문자로 들어옵니다. 문제의 수정 사항을 참고해 주세요.

- 구현 범위 및 API 사용 가능 여부

    2번 과제의 목적은 링크드 리스트를 직접 구현하는 것입니다. 따라서 리스트 관련 API는 사용하시면 안 됩니다. 그 외의 API는 사용 가능합니다. (String 관련 메소드 등)

- Recursion 사용 여부(2013-04-11 추가)

    문제의 조건에서 Recursion 관련 조건을 삭제하였습니다. Recursion을 꼭 사용하실 필요는 없습니다.

- 공백의 처리(2) (2013-04-18 추가)

    공백만 있는 문자열은 올바른 제목 및 검색어입니다. 공백도 없는 빈 문자열은 올바르지 않은 제목입니다. 즉, `INSERT %    % %      %`는 올바른 입력이며, `INSERT %GENRE% %%`, `SEARCH %%` 등은 올바르지 않은 입력입니다.
