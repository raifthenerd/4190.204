#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from subprocess import Popen, PIPE
def check(test):
    p = Popen(["java", classname], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    out, err = p.communicate(input=bytes(test,encoding))
    return out.decode(encoding)
classname = "SortingTest"
encoding = "UTF-8"
repeat = 30
size = [str(i) for i in [200000,400000,600000,800000,1000000]]
algorithms_n2 = ["B","I"]
algorithms_nlogn = ["H","M","Q","R"]
result_n2 = [a+","+n[:-1]+","+check("r "+n[:-1]+" -1000000 1000000\n"+a+"\nX\n").replace(" ms\n","")
             for n in size for a in algorithms_n2 for _ in range(repeat)]
result_nlogn = [a+","+n+","+check("r "+n+" -1000000 1000000\n"+a+"\nX\n").replace(" ms\n","")
                for n in size for a in algorithms_nlogn for _ in range(repeat)]
with open('result.csv','w') as f:
    f.write("algorithm,size,time\n")
    f.write("\n".join(result_n2+result_nlogn))
