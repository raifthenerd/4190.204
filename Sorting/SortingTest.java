import java.io.*;
import java.util.*;

public class SortingTest
{
	public static void main(String args[])
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		try
		{
			boolean isRandom = false;	// 입력받은 배열이 난수인가 아닌가?
			int[] value;	// 입력 받을 숫자들의 배열
			String nums = br.readLine();	// 첫 줄을 입력 받음
			if (nums.charAt(0) == 'r')
			{
				// 난수일 경우
				isRandom = true;	// 난수임을 표시

				String[] nums_arg = nums.split(" ");

				int numsize = Integer.parseInt(nums_arg[1]);	// 총 갯수
				int rminimum = Integer.parseInt(nums_arg[2]);	// 최소값
				int rmaximum = Integer.parseInt(nums_arg[3]);	// 최대값

				Random rand = new Random();	// 난수 인스턴스를 생성한다.

				value = new int[numsize];	// 배열을 생성한다.
				for (int i = 0; i < value.length; i++)	// 각각의 배열에 난수를 생성하여 대입
					value[i] = rand.nextInt(rmaximum - rminimum + 1) + rminimum;
			}
			else
			{
				// 난수가 아닐 경우
				int numsize = Integer.parseInt(nums);

				value = new int[numsize];	// 배열을 생성한다.
				for (int i = 0; i < value.length; i++)	// 한줄씩 입력받아 배열원소로 대입
					value[i] = Integer.parseInt(br.readLine());
			}

			// 숫자 입력을 다 받았으므로 정렬 방법을 받아 그에 맞는 정렬을 수행한다.
			while (true)
			{
				int[] newvalue = (int[])value.clone();	// 원래 값의 보호를 위해 복사본을 생성한다.

				String command = br.readLine();

				long t = System.currentTimeMillis();
				switch (command.charAt(0))
				{
					case 'B':	// Bubble Sort
						newvalue = DoBubbleSort(newvalue);
						break;
					case 'I':	// Insertion Sort
						newvalue = DoInsertionSort(newvalue);
						break;
					case 'H':	// Heap Sort
						newvalue = DoHeapSort(newvalue);
						break;
					case 'M':	// Merge Sort
						newvalue = DoMergeSort(newvalue);
						break;
					case 'Q':	// Quick Sort
						newvalue = DoQuickSort(newvalue);
						break;
					case 'R':	// Radix Sort
						newvalue = DoRadixSort(newvalue);
						break;
					case 'X':
						return;	// 프로그램을 종료한다.
					default:
						throw new IOException("잘못된 정렬 방법을 입력했습니다.");
				}
				if (isRandom)
				{
					// 난수일 경우 수행시간을 출력한다.
					System.out.println((System.currentTimeMillis() - t) + " ms");
				}
				else
				{
					// 난수가 아닐 경우 정렬된 결과값을 출력한다.
					for (int i = 0; i < newvalue.length; i++)
					{
						System.out.println(newvalue[i]);
					}
				}

			}
		}
		catch (IOException e)
		{
			System.out.println("입력이 잘못되었습니다. 오류 : " + e.toString());
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
    // modification from:
    //  Cormen, T. H., C. E. Leiserson, R. L. Rivest, and C. Stein (2009).
    //  Introduction to Algorithms (3rd ed.). The MIT Press. p. 40.
    ////////////////////////////////////////////////////////////////////////////////////////////////////
	private static int[] DoBubbleSort(int[] value)
	{
		// TODO : Bubble Sort 를 구현하라.
		// value는 정렬안된 숫자들의 배열이며 value.length 는 배열의 크기가 된다.
		// 결과로 정렬된 배열은 리턴해 주어야 하며, 두가지 방법이 있으므로 잘 생각해서 사용할것.
		// 주어진 value 배열에서 안의 값만을 바꾸고 value를 다시 리턴하거나
		// 같은 크기의 새로운 배열을 만들어 그 배열을 리턴할 수도 있다.
        for(int i = 0; i < value.length; i++) {
            for(int j = value.length-1; j > i; j--) {
                if(value[j]<value[j-1]) {
                    int temp = value[j];
                    value[j] = value[j-1];
                    value[j-1] = temp;
                }
            }
        }
		return (value);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
    // modification from:
    //  Cormen, T. H., C. E. Leiserson, R. L. Rivest, and C. Stein (2009).
    //  Introduction to Algorithms (3rd ed.). The MIT Press. p. 18.
    ////////////////////////////////////////////////////////////////////////////////////////////////////
	private static int[] DoInsertionSort(int[] value)
	{
		// TODO : Insertion Sort 를 구현하라.
        for(int j = 1; j < value.length; j++) {
            int key =  value[j];
            int i = j-1;
            while(i >= 0 && value[i] > key) {
                value[i+1] = value[i];
                i--;
            }
            value[i+1] = key;
        }
		return (value);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
    // modification from:
    //  Cormen, T. H., C. E. Leiserson, R. L. Rivest, and C. Stein (2009).
    //  Introduction to Algorithms (3rd ed.). The MIT Press. pp. 151-160.
    ////////////////////////////////////////////////////////////////////////////////////////////////////
	private static int[] DoHeapSort(int[] value)
	{
		// TODO : Heap Sort 를 구현하라.
        int size = value.length;
        for(int i = size/2-1; i>=0; i--) {
            maxHeapify(value, i, size);
        }
        for(int i = value.length-1; i > 0; i--) {
            int tmp = value[0];
            value[0] = value[i];
            value[i] = tmp;
            size--;
            maxHeapify(value, 0, size);
        }
        return value;
	}
    private static void maxHeapify(int[] heap, int i, int size) {
        int l = 2*i+1;
        int r = 2*i+2;
        int largest = i;
        if(l < size && heap[l] > heap[i]) {
            largest = l;
        }
        if(r < size && heap[r] > heap[largest]) {
            largest = r;
        }
        if(largest != i) {
            int tmp = heap[i];
            heap[i] = heap[largest];
            heap[largest] = tmp;
            maxHeapify(heap, largest, size);
        }
    }

	////////////////////////////////////////////////////////////////////////////////////////////////////
    // modification from:
    //  Cormen, T. H., C. E. Leiserson, R. L. Rivest, and C. Stein (2009).
    //  Introduction to Algorithms (3rd ed.). The MIT Press. pp. 30-34.
    ////////////////////////////////////////////////////////////////////////////////////////////////////
	private static int[] DoMergeSort(int[] value)
	{
		// TODO : Merge Sort 를 구현하라.
        mergesort(value, 0, value.length-1);
		return (value);
	}
    private static void mergesort(int[] value, int p, int r) {
        if(p<r) {
            int q = (p+r)/2;
            mergesort(value, p, q);
            mergesort(value, q+1, r);
            merge(value, p, q, r);
        }
    }
    private static void merge(int[] value, int p, int q, int r) {
        int n1 = q-p+1;
        int n2 = r-q;
        int i,j;
        int[] L = new int[n1];
        int[] R = new int[n2];
        for(i = 0; i < n1; i++) {
            L[i] = value[p+i];
        }
        for(j = 0; j < n2; j++) {
            R[j] = value[q+j+1];
        }
        i=j=0;
        for(int k = p; k <= r; k++) {
            if(i==n1) {
                value[k] = R[j++];
            } else if(j==n2) {
                value[k] = L[i++];
            } else if(L[i] <= R[j]) {
                value[k] = L[i++];
            } else {
                value[k] = R[j++];
            }
        }
    }

	////////////////////////////////////////////////////////////////////////////////////////////////////
    // modification from:
    //  Cormen, T. H., C. E. Leiserson, R. L. Rivest, and C. Stein (2009).
    //  Introduction to Algorithms (3rd ed.). The MIT Press. pp. 170-171.
    ////////////////////////////////////////////////////////////////////////////////////////////////////
	private static int[] DoQuickSort(int[] value)
	{
		// TODO : Quick Sort 를 구현하라.
        quicksort(value, 0, value.length-1);
		return (value);
	}
    private static void quicksort(int[] value, int p, int r) {
        while(p < r) {
            int q = partition(value, p, r);
            if(q-p < r-q) {
                quicksort(value, p, q-1);
                p = q+1;
            } else {
                quicksort(value, q+1, r);
                r = q-1;
            }
        }
    }
    private static int partition(int[] value, int p, int r) {
        int x = value[r];
        int i = p-1;
        for(int j = p; j < r; j++) {
            if(value[j] <= x) {
                i++;
                int tmp = value[j];
                value[j] = value[i];
                value[i] = tmp;
            }
        }
        int tmp = value[i+1];
        value[i+1] = value[r];
        value[r] = tmp;
        return i+1;
    }

	////////////////////////////////////////////////////////////////////////////////////////////////////
    // modification from:
    //  Cormen, T. H., C. E. Leiserson, R. L. Rivest, and C. Stein (2009).
    //  Introduction to Algorithms (3rd ed.). The MIT Press. pp. 194-198.
    ////////////////////////////////////////////////////////////////////////////////////////////////////
	private static int[] DoRadixSort(int[] value)
	{
		// TODO : Radix Sort 를 구현하라.
        int MAX = Math.abs(value[0]);
        for(int i = 1; i < value.length; i++) {
            if(Math.abs(value[i])>MAX) {
                MAX = Math.abs(value[i]);
            }
        }
        final int BASE = 10;
        int expr = 1;
        int[] bucket = new int[2*BASE];
        while(MAX/expr > 0) {
            int[] output = new int[value.length];
            for(int i = 0; i < bucket.length; i++) {
                bucket[i] = 0;
            }
            for(int i = 0; i < value.length; i++) {
                bucket[BASE+(value[i]/expr)%BASE-1]++;
            }
            for(int i = 1; i < bucket.length; i++) {
                bucket[i] += bucket[i-1];
            }
            for(int i = value.length-1; i >= 0; i--) {
                output[--bucket[BASE+(value[i]/expr)%BASE-1]] = value[i];
            }
            value = output;
            expr *= BASE;
        }
		return (value);
	}
}
